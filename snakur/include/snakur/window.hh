/**
 *  @file window.hh
 *  @author Olivier Détour (detour.olivier@gmail.com)
 */
#ifndef SNAKUR_WINDOW_HH_
# define SNAKUR_WINDOW_HH_

# include <ncurses.h>

# include <stdexcept>

namespace snakur {

/**
 * @brief 
 */
class window
{
  public:
    window():
      x_(0), y_(0)
    {
      initscr();

      if(!has_colors())
      {
        endwin();
        throw std::runtime_error("terminal does not support color");
      }

      noecho();
      keypad(stdscr, true);
      start_color();
      curs_set(0);

      //init_pair(1, COLOR_RED, COLOR_BLACK);    // must match enum class Color
      //init_pair(2, COLOR_GREEN, COLOR_BLACK);
      //init_pair(3, COLOR_WHITE, COLOR_BLACK);

      getmaxyx(stdscr, y_, x_);
    }

    ~window()
    {
      endwin();     // destructor restore console at exit
    }

  public:
    int x() const noexcept { return x_; }
    int y() const noexcept { return y_; }

  private:
    int x_;
    int y_;
};

} /** !snakur */

#endif /** !SNAKUR_WINDOW_HH_ */
