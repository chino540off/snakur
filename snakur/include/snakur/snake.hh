/**
 *  @file snake.hh
 *  @author Olivier Détour (detour.olivier@gmail.com)
 */
#ifndef SNAKUR_SNAKE_HH_
# define SNAKUR_SNAKE_HH_

# include <list>
# include <snakur/map.hh>

namespace snakur {

/**
 * @brief 
 */
class snake
{
  public:
    typedef std::pair<std::size_t, size_t> coord_t;

  public:
    snake(map & m):
      dir_({1, 0}),
      m_(m),
      is_dead_(false)
    {
      reset();
    }

  public:
    void reset()
    {
      is_dead_ = false;
      body_.clear();

      for (std::size_t i = 1; i <= 5; ++i)
        body_.push_front({i + m_.w() / 2, m_.h() / 2});

      update();
    }

  public:
    void update()
    {
      for (auto it: body_)
        m_.set(it.first, it.second, area::snake);
    }

  private:
    unsigned int move(coord_t dir)
    {
      auto head = body_.front();
      auto tail = body_.back();

      unsigned int ret = 0;

      // go back ?
      if (dir_.first  + dir.first  == 0 &&
          dir_.second + dir.second == 0)
        return ret;

      if (is_dead_)
        return ret;

      // can pass ?
      if (m_.bloc(head.first + dir.first,
                  head.second + dir.second))
      {
        is_dead_ = true;
        return ret;
      }
      
      // is there a fruit?
      if (m_.get(head.first + dir.first,
                 head.second + dir.second) != area::fruit)
      {
        body_.pop_back();
        m_.set(tail.first, tail.second, area::empty);
      }
      else
      {
        ret = 1;
      }

      // move head
      body_.push_front({head.first + dir.first, head.second + dir.second});

      for (auto it: body_)
        m_.set(it.first, it.second, area::snake);

      dir_ = dir;

      return ret;
    }

  public:
    unsigned int up()   { return move({ 0, 1}); }
    unsigned int down() { return move({ 0,-1}); }
    unsigned int left() { return move({-1, 0}); }
    unsigned int right(){ return move({ 1, 0}); }
    unsigned int nop()  { return move(dir_);    }

  public:
    bool is_dead() const { return is_dead_; }

  private:
    std::list<coord_t> body_;
    coord_t dir_;
    map & m_;
    bool is_dead_;
};

} /** !snakur */

#endif /** !SNAKUR_SNAKE_HH_ */
