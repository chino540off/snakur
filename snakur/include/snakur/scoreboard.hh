/**
 *  @file scoreboard.hh
 *  @author Olivier Détour (detour.olivier@gmail.com)
 */
#ifndef SNAKUR_SCOREBOARD_HH_
# define SNAKUR_SCOREBOARD_HH_

namespace snakur {

/**
 * @brief 
 */
class scoreboard
{
  public:
    scoreboard(): score_(0), dead_(false) { }

  public:
    void reset() { score_ = 0; }
    void inc(unsigned int v) { score_ += v; }
    void dead(bool v) { dead_ = v; }

  public:
    void show()
    {
      move(0, 0);
      printw(" | Score: %u | Status: %s |",
             score_, dead_ ? " Dead" : "Alive");
    }

  private:
    unsigned int score_;
    bool dead_;
};

} /** !snakur */

#endif /** !SNAKUR_SCOREBOARD_HH_ */
