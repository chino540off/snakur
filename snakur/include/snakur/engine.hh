/**
 *  @file controllers.hh
 *  @author Olivier Détour (detour.olivier@gmail.com)
 */
#ifndef SNAKUR_ENGINE_HH_
# define SNAKUR_ENGINE_HH_

# include <utility>

# include <utils/pqueue.hh>

namespace snakur {

enum class event
{
  nop,
  up,
  down,
  left,
  right,
  reset,
  end,
};

template <typename T>
struct engine
{
  template <typename ... Args>
  engine(Args && ... args):
    handler_(std::forward<Args>(args)...),
    stop_(false)
  {
  }

  template <typename P, typename M, typename S, typename SB>
  void process(P p, M & m, S & s, SB & sb)
  {
    utils::pqueue<snakur::event> q;
    std::thread t([&]() { handler_.process(q); });

    m.generate_fruit();

    while (!stop_)
    {
      auto e = q.pop(p, snakur::event::nop);
      unsigned int score = 0;

      switch (e)
      {
        case event::up:
          score = s.up();
          break;

        case event::down:
          score = s.down();
          break;

        case event::left:
          score = s.left();
          break;

        case event::right:
          score = s.right();
          break;

        case event::nop:
          score = s.nop();
          break;

        case event::end:
          stop();
          break;

        case event::reset:
          m.reset();
          s.reset();
          sb.reset();
          m.generate_fruit();
          break;
      }

      if (score)
        m.generate_fruit();

      sb.inc(score);
      sb.dead(s.is_dead());

      // Draw
      m.show();
      sb.show();
      refresh();
    }

    t.join();
  }

  void stop()
  {
    handler_.stop();
    stop_ = true;
  }

  T handler_;
  bool stop_;
};


} /** !snakur */

#endif /** !SNAKUR_ENGINE_HH_ */
