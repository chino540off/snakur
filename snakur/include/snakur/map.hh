/**
 *  @file map.hh
 *  @author Olivier Détour (detour.olivier@gmail.com)
 */
#ifndef SNAKUR_MAP_HH_
# define SNAKUR_MAP_HH_

# include <iostream>
# include <vector>

# include <spdlog/fmt/ostr.h>

# include <snakur/window.hh>

namespace snakur {

enum class area
{
  empty = 0,
  wall,
  snake,
  fruit,
};

/**
 * @brief 
 */
class map
{
  public:
    map(std::size_t w,
        std::size_t h):
      w_(w),
      h_(h),
      m_(w_ * h_),
      w_gen_(0, w_ - 1),
      h_gen_(0, h_ - 1)
    {
      rng.seed(std::random_device()());

      init_pair(static_cast<std::underlying_type_t<area>>(area::empty), COLOR_BLACK, COLOR_BLACK);
      init_pair(static_cast<std::underlying_type_t<area>>(area::wall),  COLOR_WHITE, COLOR_WHITE);
      init_pair(static_cast<std::underlying_type_t<area>>(area::snake), COLOR_GREEN, COLOR_GREEN);
      init_pair(static_cast<std::underlying_type_t<area>>(area::fruit), COLOR_RED,   COLOR_RED);
      reset();
    }

  public:
    void reset()
    {
      iter([&](std::size_t x, std::size_t y)
      {
        set(x, y, area::empty);

        if (x == 0 || x == w_ - 1 ||
            y == 0 || y == h_ - 1)
          set(x, y, area::wall);
      });
    }

  public:
    void generate_fruit()
    {
      while (true)
      {
        std::size_t x = w_gen_(rng);
        std::size_t y = h_gen_(rng);

        if (get(x, y) == area::empty)
        {
          set(x, y, area::fruit);
          return;
        }
      }
    }

    bool bloc(std::size_t x, std::size_t y)
    {
      return get(x, y) == area::wall || get(x, y) == area::snake;
    }

  public:
    void show()
    {
      move(1, 0);

      iter([&](std::size_t x, std::size_t y)
      {
        attron(COLOR_PAIR(static_cast<std::underlying_type_t<area>>(get(x, y))));
        printw("  ");
        attroff(COLOR_PAIR(static_cast<std::underlying_type_t<area>>(get(x, y))));

        if (x == w_ - 1)
          printw("\n");
      });
    }

  public:
    template <typename F>
    void iter(F && f)
    {
      for (std::size_t y = 0; y < h_; ++y)
        for (std::size_t x = 0; x < w_; ++x)
          f(x, h_ - y - 1);
    }

    void set(std::size_t x,
             std::size_t y,
             area v)
    {
      m_[index(x, y)] = v;
    }

    area get(std::size_t x,
             std::size_t y) const
    {
      return m_[index(x, y)];
    }

  private:
    std::size_t index(std::size_t x, std::size_t y) const
    {
      auto idx = x * w_ + y;

      assert(idx < m_.size());

      return idx;
    }

  public:
    auto w() const { return w_; }
    auto h() const { return h_; }

  private:
    const std::size_t w_;
    const std::size_t h_;

    std::vector<area> m_;

    std::mt19937 rng;
    std::uniform_int_distribution<std::mt19937::result_type> w_gen_;
    std::uniform_int_distribution<std::mt19937::result_type> h_gen_;
};

} /** !snakur */

#endif /** !SNAKUR_MAP_HH_ */
