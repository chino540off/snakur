/**
 *  @file keyboard.hh
 *  @author Olivier Détour (detour.olivier@gmail.com)
 */
#ifndef SNAKUR_CONTROLLERS_KEYBOARD_HH_
# define SNAKUR_CONTROLLERS_KEYBOARD_HH_

# include <ncurses.h>

namespace snakur {
namespace controllers {

struct kb
{
  kb(): stop_(false) {}

  template <typename Q>
  void process(Q & q)
  {
    while (!stop_)
    {
      int key_ = getch();

      switch (key_)
      {
        case KEY_UP:
          q.push(snakur::event::up);
          break;
        case KEY_DOWN:
          q.push(snakur::event::down);
          break;
        case KEY_LEFT:
          q.push(snakur::event::left);
          break;
        case KEY_RIGHT:
          q.push(snakur::event::right);
          break;
        case 'r':
          q.push(snakur::event::reset);
          break;
        case 'q':
          q.push(snakur::event::end);
          stop();
          return;
      }
    }
  }

  void stop() { stop_ = true; }

  bool stop_;
};

} /** !controllers */
} /** !snakur */

#endif /** !SNAKUR_CONTROLLERS_KEYBOARD_HH_ */
