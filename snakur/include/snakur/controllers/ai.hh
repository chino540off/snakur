/**
 *  @file ai.hh
 *  @author Olivier Détour (detour.olivier@gmail.com)
 */
#ifndef SNAKUR_CONTROLLERS_AI_HH_
# define SNAKUR_CONTROLLERS_AI_HH_

# include <utils/neural-net.hh>

namespace snakur {
namespace controllers {

/**
 * @brief 
 */
class ai
{
  public:
    ai(): stop_(false) {}

  public:
    template <typename Q>
    void process(Q & q)
    {
    }

  public:
    void stop() { stop_ = true; }

  private:
    bool stop_;
};

} /** !controllers */
} /** !snakur */

#endif /** !SNAKUR_CONTROLLERS_AI_HH_ */
