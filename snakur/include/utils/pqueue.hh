/**
 *  @file pqueue.hh
 *  @author Olivier Détour (detour.olivier@gmail.com)
 */
#ifndef UTILS_PQUEUE_HH_
# define UTILS_PQUEUE_HH_

# include <chrono>
# include <condition_variable>
# include <mutex>
# include <thread>
# include <queue>

namespace utils {

template <typename T>
class pqueue
{
  public:
    template <typename P>
    T pop(P p, T def)
    {
      std::unique_lock<std::mutex> mlock(mutex_);

      if (cond_.wait_for(mlock, p, [&]() { return !queue_.empty(); }))
      {
        auto item = queue_.front();
        queue_.pop();
        return item;
      }
      else
      {
        return def;
      }
    }
   
    void push(T const & item)
    {
      std::unique_lock<std::mutex> mlock(mutex_);

      queue_.push(item);
      mlock.unlock();
      cond_.notify_one();
    }
   
  private:
    std::queue<T> queue_;
    std::mutex mutex_;
    std::condition_variable cond_;
};

} /** !utils */

#endif /** !UTILS_PQUEUE_HH_ */
