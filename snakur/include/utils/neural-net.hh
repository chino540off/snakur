/**
 *  @file neural-net.hh
 *  @author Olivier Détour (detour.olivier@gmail.com)
 */
#ifndef UTILS_NEURAL_NET_HH_
# define UTILS_NEURAL_NET_HH_

# include <vector>

namespace utils {
namespace neural {

class neuron;

typedef std::vector<neuron> layer;

struct connection
{
  double w;
  double dw;
};

class neuron
{
  public:
    neuron(unsigned numOutputs, unsigned myIndex);
    void setOutputVal(double val) { output_val_ = val; }
    double getOutputVal(void) const { return output_val_; }
    void feedForward(const layer &prevLayer);
    void calcOutputGradients(double targetVals);
    void calcHiddenGradients(const layer &nextLayer);

    void updateInputWeights(layer & p_layer)
    {
      // The weights to be updated are in the Connection container
      // in the nuerons in the preceding layer

      for(auto & n : p_layer)
      {
        double oldDeltaWeight = n.m_outputWeights[index_].dw;

        // Individual input, magnified by the gradient and train rate:
        // Also add momentum = a fraction of the previous delta w
        double newDeltaWeight = eta * n.getOutputVal() * gradient_ + alpha * oldDeltaWeight;

        n.m_outputWeights[index_].dw = newDeltaWeight;
        n.m_outputWeights[index_].w += newDeltaWeight;
      }
    }

  private:
    static double eta; // [0.0...1.0] overall net training rate
    static double alpha; // [0.0...n] multiplier of last weight change [momentum]
    static double transferFunction(double x);
    static double transferFunctionDerivative(double x);

    // randomWeight: 0 - 1
    static double randomWeight()
    {
      return rand() / double(RAND_MAX);
    }

    double sumDOW(layer const & n_layer) const
    {
      double sum = 0.0;

      // Sum our contributions of the errors at the nodes we feed
      for (unsigned n = 0; n < n_layer.size(); ++n)
      {
        sum += m_outputWeights[n].w * n_layer[n].gradient_;
      }

      return sum;
    }

    double output_val_;

    std::vector<connection> m_outputWeights;
    unsigned index_;
    double gradient_;
};

class net
{
  public:

};

} /** !neural */
} /** !utils */

#endif /** !UTILS_NEURAL_NET_HH_ */
