/**
 *  @file signal.hh
 *  @author Olivier Détour (detour.olivier@gmail.com)
 */
#ifndef UTILS_SIGNAL_HH_
# define UTILS_SIGNAL_HH_

# include <functional>
# include <csignal>

namespace utils {

template <int q>
struct signal
{
  using sfx = void(int);
  typedef std::function<void(int)> fx_t;

  fx_t fx;

  static signal holder;
  static void handler(int sn) { holder.fx(sn); }
};
template <int q>
signal<q> signal<q>::holder;

// this is a scope
template <int q>
struct signal_scope
{
  using sfx = void(int);
  sfx * oldfx_;

  signal_scope(typename signal<q>::fx_t fx)
  {
    signal<q>::holder.fx = fx;
    oldfx_ = std::signal(q, &signal<q>::handler);
  }

  ~signal_scope()
  {
    std::signal(q, oldfx_);
  }
};

} /** !utils */

#endif /** !UTILS_SIGNAL_HH_ */
