#include <utils/signal.hh>

#include <snakur/map.hh>
#include <snakur/snake.hh>
#include <snakur/engine.hh>
#include <snakur/scoreboard.hh>
#include <snakur/controllers/keyboard.hh>

using namespace std::chrono_literals;

int main()
{
  snakur::window win;
  snakur::map m(50, 50);
  snakur::snake s(m);
  snakur::scoreboard sb;

  snakur::engine<snakur::controllers::kb> e;

  utils::signal_scope<SIGINT>([&](int) { e.stop(); });

  e.process(100ms, m, s, sb);
}
