# SNAKUR

`snakur`, _snake_ in English, is another snake game implementation with `ncurses`.

## Build

To build, use a default `cmake`build procedure.

``` sh
mkdir build
cd build
cmake ..
make
```

## Run

To run `snakur`

``` sh
./snakur/snakur
```